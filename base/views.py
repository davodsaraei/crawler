from django.shortcuts import render

from django.shortcuts import render_to_response
from django.http import JsonResponse
from django.utils.translation import gettext as _
from django.http import HttpResponse
import json


def index(request):
    return render(request, 'index.html')

def search(request):
    title = request.POST.get('searchedValue','عنوان')
    

    description = """لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت
     چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی
      مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.              
    """
    link    = "http://loremsaz.com"
    my_list = []

    for i in list(range(10)):
        my_list += [
            {
                'title': title,
                'link': link,
                'description': description
            },
        ]
    
    return render(request, 'search.html', {
            'searchedValue':title,
            'my_list':my_list
        })

def saveRss(request):
    if request.method == 'GET':
        return __getSaveRss(request)
    elif request.method == 'POST':
        return __postSaveRss(request)
    
    return JsonResponse({
        'status': 'NOk',
        'message': _('متد قابل قبول نیست')
    })


def __getSaveRss(request):
    return render(request, 'save_rss.html')

def __postSaveRss(request):
    link = request.POST.get('link')

    #result = elasticsearch.download(json=link)

    #elasticsearch.put(index='', json='')
    
    #elasticsearch.search(index='', json='dawda')
    
    return JsonResponse({
        'status': 'Ok',
        'message': _(' لینک ذخیره شد.')
    })
