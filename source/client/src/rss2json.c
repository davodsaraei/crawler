#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//Compare 2 strings
static int strsafecmp(const char* dst, const char* src, size_t n){
	if(strlen(dst)>=n && strlen(src)>=n && strncmp(dst, src, n)==0)
		return 1;
	else
		return 0;

}

//Convert <tag name1="value1" name2="value2">value3</tag> to "tag":{"name1":"value1","name2":"value1","tag":"value3"}
static int parse_inline(char* src, FILE* fpw , const char* skey ,const char* r_skey, const char* ekey){
	char* src_bak=src;
	if(strsafecmp(src,skey, strlen(skey)-1) && src[strlen(skey)-1]==' '){
		src+=strlen(skey);
		fwrite(r_skey,1,strlen(r_skey)-1,fpw);
		fwrite("{",1,1,fpw);
		while(src[0]!='\0' && src[0]!= '>' && !(src[0]=='/' && src[1]=='>')){
			if(src[0]!=' '){
				fwrite("\"",1,1,fpw);
				while(src[0]!='\0' && src[0]!='=' && src[0]!=' '){
					fwrite(src,1,1,fpw);
					src++;
				}
				fwrite("\":\"",1,3,fpw);
				while(src[0]!='\"')
					src++;
				src++;
				while(src[0]!='\0' && src[0]!='\"'){
					fwrite(src,1,1,fpw);
					src++;
				}
				fwrite("\",",1,2,fpw);
			}
			src++;
		}
		if(src[0]=='>'){
			src++;
			fwrite(r_skey,1,strlen(r_skey),fpw);
			while(!strsafecmp(src,ekey,strlen(ekey))){
				fwrite(src,1,1,fpw);
				src++;
			}
			fwrite("\"},",1,3,fpw);
		}
		else if(src[0]=='/' && src[1]=='>'){
			src+=2;
			fseek(fpw,-1,SEEK_CUR);
			fwrite("},",1,2,fpw);
		}		
		return (src-src_bak);
	}
return 0;
}

//Shift data buffer
static void inc_next(FILE* fp, char* data, int num){
	for(int i=0; i< num; i++){
		for(size_t j=0; j< strlen(data); j++)
			data[j]=data[j+1];

		if(!feof(fp)){
			char buffer[1];
			if(fread(buffer, 1, 1, fp))
				data[strlen(data)]=buffer[0];
		}
	}
}

//Generate an array of items
static int array_gen(FILE* input, const char* json){
	
	char* key="\"item\":";
	FILE* outfile=fopen(json,"w");
	rewind(input);
	if(ferror(outfile)){
		perror(json);
		return 0;
	}
	char* data=malloc(101);
	if(data==NULL){
		fprintf(stderr,"Memory allocation faild\n");
		exit(1);
	}
	int read_size=fread(data, 1 , 100,input);
	data[read_size]='\0';
	if(read_size<10){
		free(data);
		return 0;
	}
	int count_in=0;
	int count_out=0;
	while(data[0]!='\0' && strncmp(data,key,strlen(key))!=0){
		fwrite(data,1,1,outfile);
		inc_next(input,data,1);
	}
	inc_next(input,data,strlen(key));
	fwrite(key,1,strlen(key),outfile);
	fwrite("[",1,1,outfile);
	while(data[0]!='\0'){
		if(strncmp(data,key,strlen(key))==0){
			inc_next(input,data,strlen(key));
		}
		else if(strncmp(data,"{",1)==0){
			fwrite(data,1,1,outfile);
			inc_next(input,data,1);
			count_in++;
		}
		else if(strncmp(data,"}",1)==0){
			fwrite(data,1,1,outfile);
			inc_next(input,data,1);
			count_out++;
		}
		else{
			fwrite(data,1,1,outfile);
			inc_next(input,data,1);
		}

		if(count_out==count_in+1){
			fseek(outfile,-1,SEEK_CUR);
			fwrite("]}",1,2,outfile);
			break;
		}
	}
	while(data[0]!='\0'){
		fwrite(data,1,1,outfile);
		inc_next(input,data,1);
	}
	fclose(outfile);
	free(data);
	return 1;

}

//Main function
int rss2json(const char* rss,const char* json){

	//Opening files
	FILE* fp=fopen(rss,"rb");
	if(ferror(fp)){
		perror(rss);
		return 1;
	}
	FILE* fpw=tmpfile();
	if(ferror(fpw)){
		perror(json);
		return 1;
	}

	//Load buffer
	char* data=malloc(301);
	if(data==NULL){
		fprintf(stderr,"Memory allocation faild\n");
		exit(1);
	}
	int read_size=fread(data, 1 , 300,fp);
	data[read_size]='\0';
	if(read_size<10){
		free(data);
		return 1;
	}

	//Sub-elements of item: You can add your new tags to array below, **Last argument must NULL**
	char* keys[]={"title", "link", "description" , "pubDate", "guid",
	"author", "comments","category","source","enclosure","url","width","height","language",NULL};
	
	//skey: <tag> | ekey: </tag> | r_skey: "tag":" | r_ekey: ",  
	int keylen=sizeof(keys)/sizeof(keys[0]);

	char* skey[sizeof(keys)/sizeof(keys[0])];
	char* ekey[sizeof(keys)/sizeof(keys[0])];
	char* r_skey[sizeof(keys)/sizeof(keys[0])];
	char* r_ekey[sizeof(keys)/sizeof(keys[0])];
	
	for(int i=0; i<keylen-1; i++){
		int key_size=strlen(keys[i]);
	
		skey[i]=malloc(key_size+3);
		if(skey[i]==NULL){
			fprintf(stderr,"Memory allocation faild\n");
			exit(1);
		}
		strcpy(skey[i],"<");
		strncat(skey[i],keys[i],key_size);
		strncat(skey[i],">",1);
		
		ekey[i]=malloc(key_size+4);
		if(ekey[i]==NULL){
			fprintf(stderr,"Memory allocation faild\n");
			exit(1);
		}
		strcpy(ekey[i],"</");
		strncat(ekey[i],keys[i],key_size);
		strncat(ekey[i],">",1);
	
		r_skey[i]=malloc(key_size+5);
		if(r_skey[i]==NULL){
			fprintf(stderr,"Memory allocation faild\n");
			exit(1);
		}
		strcpy(r_skey[i],"\"");
		strncat(r_skey[i],keys[i],key_size);
		strncat(r_skey[i],"\":\"",3);
	
		r_ekey[i]="\",";
	}
	skey[keylen-1]=ekey[keylen-1]=r_skey[keylen-1]=r_ekey[keylen-1]=NULL;
	
	//First argument: channel | Other arguments: sub-elements of channel
	char* gskey[]={"<channel>","<item>","<image>",NULL};
	char* gekey[]={"</channel>","</item>","</image>",NULL};
	char* gr_skey[]={"\"channel\":{","\"item\":{","\"image\":{",NULL};
	char* gr_ekey[]={"},","},","},",NULL};
	
	fwrite("{",1,1,fpw);
	for(int passed=0; data[0]!='\0'; inc_next(fp,data,1)){
		if(strsafecmp(data ,gskey[0], strlen(gskey[0]))){
				inc_next(fp,data,strlen(gskey[0]));
			fwrite(gr_skey[0],1,strlen(gr_skey[0]),fpw);
	
			while(data[0]!='\0' && !strsafecmp(data, gekey[0], strlen(gekey[0]))){
				for(int gstep=1; gskey[gstep]!=NULL; gstep++){
					if(strsafecmp(data ,gskey[gstep], strlen(gskey[gstep]))){
							inc_next(fp,data,strlen(gskey[gstep]));
						fwrite(gr_skey[gstep],1,strlen(gr_skey[gstep]),fpw);
					}
			}
			while(data[0]!='\0' &&  !strsafecmp(data, gekey[0], strlen(gekey[0])) &&
						!strsafecmp(data, gekey[1], strlen(gekey[1])) &&
						!strsafecmp(data, gekey[2], strlen(gekey[2])) ){
					for(int gstep=1; gskey[gstep]!=NULL; gstep++){
						if(strsafecmp(data ,gskey[gstep], strlen(gskey[gstep]))){
								inc_next(fp,data,strlen(gskey[gstep]));
							fwrite(gr_skey[gstep],1,strlen(gr_skey[gstep]),fpw);
						}
					}
					for(int step=0; skey[step]!=NULL ; step++){
						if(strsafecmp(data ,skey[step], strlen(skey[step]))){
							passed=1;
								inc_next(fp,data,strlen(skey[step]));
							fwrite(r_skey[step],1,strlen(r_skey[step]),fpw);
							while(data[0]!='\0' && !strsafecmp(data, ekey[step], strlen(ekey[step]))){
								switch(data[0]){
									case '\"':
										fwrite("\\\"",1,2,fpw);
										inc_next(fp,data,1);
										break;
									case '\\':
										fwrite("\\\\",1,2,fpw);
										inc_next(fp,data,1);
										break;
									case '\t':
										fwrite("\\t",1,2,fpw);
										inc_next(fp,data,1);
										break;
									case '\n':
										fwrite("\\n",1,2,fpw);
										inc_next(fp,data,1);
										break;
									case '\r':
										fwrite("\\r",1,2,fpw);
										inc_next(fp,data,1);
										break;
									case '\f':
										fwrite("\\f",1,2,fpw);
										inc_next(fp,data,1);
										break;
									case '\b':
										fwrite("\\b",1,2,fpw);
										inc_next(fp,data,1);
										break;
									default:
										fwrite(data,1,1,fpw);
										inc_next(fp,data,1);
								}
							}
							fwrite(r_ekey[step],1,strlen(r_ekey[step]),fpw);
						}
						else{
							int count=parse_inline(data,fpw,skey[step],r_skey[step],ekey[step]);
							if(count){
								passed=1;
									inc_next(fp,data,count);
							}
						}
					}
					inc_next(fp,data,1);
				}
				if(passed){
					fseek(fpw,-1,SEEK_CUR);
					passed=0;
				}
				for(int gstep=1; gskey[gstep]!=NULL; gstep++){
					if(strsafecmp(data ,gekey[gstep], strlen(gekey[gstep])))
						fwrite(gr_ekey[gstep],1,strlen(gr_ekey[gstep]),fpw);
				}
				inc_next(fp,data,1);
			}
		fseek(fpw,-1,SEEK_CUR);
		fwrite(gr_ekey[0],1,strlen(gr_ekey[0]),fpw);
		}
	}
	fseek(fpw,-1,SEEK_CUR);
	fwrite("}",1,1,fpw);
	
	free(data);
	if(!array_gen(fpw,json)){
		fclose(fp);
		fclose(fpw);
		return 1;
	}
	for(int i=0; i<keylen-1; i++){
		free(skey[i]);
		free(ekey[i]);
		free(r_skey[i]);
	}
	fclose(fp);
	fclose(fpw);
	return 0;
}
