#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <string.h>
#include "rss2json.h"

#define URL_DEF "127.0.0.1:9200"

char* unencode(char *src){
	char* dst= malloc(strlen(src));
	if(dst==NULL){
		fprintf(stderr,"Memory allocation faild\n");
		exit(1);
	}
	int j=0;
	for(int i=0; src[i] != '\0'; i++ , j++)
		if(src[i] == '+')
			dst[j] = ' ';
		else if(src[i] == '%' && src[i+1]!='0' && src[i+2]!='0'){
			unsigned int code;
			if(sscanf(src+i+1, "%2x", &code) != 1) code = '?';
			dst[j] = code;
			i +=2;
		}
		else
			dst[j] = src[i];
	dst[j] = '\0';
	return dst;
}
CURLcode curl_send(struct curl_slist *headers , const char* url , const char* method , const char* json , FILE* file){
		curl_global_init(CURL_GLOBAL_ALL);
		CURL *curl= curl_easy_init();
		CURLcode res;
		if(curl){
			if(file!=NULL){
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, (FILE*) file);
				}
			if(headers!=NULL)
				curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
			if(json!=NULL)
				curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json);
			curl_easy_setopt(curl, CURLOPT_URL, url);
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);

		}
		res=curl_easy_perform(curl);

		curl_easy_cleanup(curl);
		curl_global_cleanup();
		return res;

}
void curl_func(char* method, char* index, char* json){
	if(method==NULL)
		return;

	char* url=malloc(strlen(URL_DEF)+strlen(index)+10);
	if(url==NULL){
		fprintf(stderr,"Memory allocation faild\n");
		exit(1);
	}
	strcpy(url,URL_DEF);
	CURLcode res;
	struct curl_slist *headers=NULL;
	struct curl_slist *bulk_headers=NULL;
	headers=curl_slist_append(headers, "Content-Type: application/json");
	bulk_headers=curl_slist_append(bulk_headers, "Content-Type: application/x-ndjson");

	for(size_t i=0; i< strlen(json); i++){
		if(json[i]=='\n'){
			headers=bulk_headers;
			break;
		}
	}
	if(index[0]!='/')
		strcat(url,"/");
	strcat(url,index);

	if(strcmp(method, "put")==0){
		res=curl_send(headers,url,"PUT",json,NULL);
	}
	else if(strcmp(method, "get")==0){
		res=curl_send(headers,url,"GET",json,NULL);
	}
	else if(strcmp(method, "post")==0){
		res=curl_send(headers,url,"POST",json,NULL);
	}
	else if(strcmp(method, "search")==0){
		strcat(url,"/_search");
		res=curl_send(headers,url,"GET",json,NULL);
	}
	else if(strcmp(method, "flush")==0){
		strcat(url,"/_flush");
		res=curl_send(NULL,url,"POST",NULL,NULL);
	}
	else if(strcmp(method, "bulk")==0){
		strcat(url,"/_bulk");
		res=curl_send(bulk_headers,url,"POST",json,NULL);
	}
	else if(strcmp(method, "download")==0){
		FILE* rssfile=fopen("rss.txt","w");
		res=curl_send(NULL,json,"GET",NULL,rssfile);
		fclose(rssfile);
		if(rss2json("rss.txt","json.txt")==0){
			FILE* jsonfile=fopen("json.txt","rb");
			char jsonbuff[1];
			while(fread(jsonbuff,1,1,jsonfile)){
				printf("%c",jsonbuff[0]);
			}
			fclose(jsonfile);
		}
		else{	
			curl_slist_free_all(headers);
			free(url);
			return;
		}
	}
	else if(strcmp(method, "delete")==0){
		res=curl_send(NULL,url,"DELETE",NULL,NULL);
	}
	
	if(res!=CURLE_OK){
		perror(url);
	}

	curl_slist_free_all(headers);
	free(url);

}

int main(int argc, char* argv[]){

	char* json=argv[3];
	char* index=argv[2];
	char* method=argv[1];

	curl_func(method,index,json);
	fflush(stdout);

	return 0;

}
