import subprocess
class elasticsearch:
    @staticmethod
    def raw(method,index=" ",json=" "):
        program="client.exe"
        result=""
        resultfile=open("result.txt","w",encoding='utf8')
        p=subprocess.Popen([program,method,index,json],stdout=resultfile)
        p.wait()
        resultfile.close()
        resultfile=open("result.txt","r",encoding='utf8')
        result=resultfile.read()
        resultfile.close()
        return result
    @staticmethod
    def put(index,json):
        method="put"
        return elasticsearch.raw(method,index,json)
    @staticmethod
    def get(index,json):
        method="get"
        return elasticsearch.raw(method,index,json)
    @staticmethod
    def post(index,json):
        method="post"
        return elasticsearch.raw(method,index,json)
    @staticmethod
    def search(index,json):
        method="search"
        return elasticsearch.raw(method,index,json)
    @staticmethod
    def search_byvalue(index,tag,value):
        method="search"
        min_doc_freq="1"
        min_term_freq="1"
        min_word_length="3"
        if (tag=="") or (tag==None):
            json="{\"query\":{\"more_like_this\":{\"like\": \"" +value+ "\", \"min_term_freq\": "+min_term_freq+" ,\"min_doc_freq\": "+min_doc_freq+", \"min_word_length\": "+min_word_length+"}}}"
        else:
            tagstr='","'.join(tag)
            tagstr="\""+tagstr+"\""
            json="{\"query\":{\"more_like_this\":{\"fields\": ["+tagstr+"],\"like\": \"" +value+ "\", \"min_term_freq\": "+min_term_freq+" ,\"min_doc_freq\": "+min_doc_freq+", \"min_word_length\": "+min_word_length+"}}}"
        return elasticsearch.raw(method,index,json)
    @staticmethod
    def flush(index):
        method="flush"
        return elasticsearch.raw(method,index," ")
    @staticmethod
    def bulk(index,json):
        method="bulk"
        return elasticsearch.raw(method,index,json)
    @staticmethod
    def download(json):
        method="download"
        return elasticsearch.raw(method," ",json)
    @staticmethod
    def delete(index):
        method="delete"
        return elasticsearch.raw(method,index," ")

##EXAMPLES:

## Download RSS -> convert RSS to JSON -> JSON returned
	#p=elasticsearch.download(json="https://www.nasa.gov/rss/dyn/Gravity-Assist.rss")

## Insert {"name": "NASA"} in nasa/1
	#p=elasticsearch.post(index="nasa/1",json="{\"name\": \"NASA\"}") 

## Search "Cornell" in description and title tags exist in index(nasa)
## NOTICE: tag must be an array!
	#p=elasticsearch.search_byvalue(index="nasa",tag=["description","title"],value="Cornell")   

## Search "Cornell" in all tags exist in index(nasa)
	#p=elasticsearch.search_byvalue(index="nasa",tag="",value="Cornell")

## Delete index "nasa/1"
	#p=elasticsearch.delete("nasa/1")

## Delete all(setting, index) in "nasa"
##Warning: Try not to delete "/" or "" because it will delete settings too!
	#p=elasticsearch.flush("nasa")
