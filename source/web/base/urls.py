from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path(r'^$',views.index, name='index'),
    re_path(r'^search/$',views.search, name='search'),
    re_path(r'^save-rss/$',views.saveRss, name='saveRss')
]