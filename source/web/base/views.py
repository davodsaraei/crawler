from django.shortcuts import render

from django.shortcuts import render_to_response
from django.http import JsonResponse
from django.utils.translation import gettext as _
from django.http import HttpResponse
import json
from .elasticsearch.clientapi import elasticsearch
import time

def index(request):
    return render(request, 'index.html')

def search(request):
    title = request.POST.get('searchedValue','عنوان')

    result=elasticsearch.search_byvalue(index="_all",tag=["description","title"], value=title)
    jsondata=json.loads(result)
    
    my_list = []
    j=0
    for i in jsondata["hits"]["hits"]:
        my_list += [
            {
                'title': jsondata["hits"]["hits"][j]["_source"]["title"],
                'link': jsondata["hits"]["hits"][j]["_source"]["link"],
                'description': jsondata["hits"]["hits"][j]["_source"]["description"]
            },
        ]
        j=j+1
    
    return render(request, 'search.html', {
            'searchedValue':title,
            'my_list':my_list
        })

def saveRss(request):
    if request.method == 'GET':
        return __getSaveRss(request)
    elif request.method == 'POST':
        return __postSaveRss(request)
    
    return JsonResponse({
        'status': 'NOk',
        'message': _('متد قابل قبول نیست')
    })


def __getSaveRss(request):
    return render(request, 'save_rss.html')

def __postSaveRss(request):
    link = request.POST.get('link')

    result=elasticsearch.download(json=link)
    jsondata=json.loads(result)
    link2=link
    for i in ["http:","https:","?","&","*"," ","%","/",",","\\",":",".","^","$","#","!",";","\"","\'","-","_"]:
        link2=link2.replace(i,"")
    j=0;
    for i in jsondata["channel"]["item"]:
        data2pass=jsondata["channel"]["item"][j]
        elasticsearch.post(index="rss/"+link2,json=json.dumps(data2pass))
        j=j+1
    
    return JsonResponse({
        'status': 'Ok',
        'message': _(' لینک ذخیره شد.')
    })
